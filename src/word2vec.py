from gensim.models import KeyedVectors
from difflib import get_close_matches


MODEL_PATH = "./models/google-news-word2vec-negative300.bin"


class Word2Vec():
    """
    Wrapper class around the real model, for
    easier, more intuitive usage
    """

    def __init__(self):
        # init data
        self.data = KeyedVectors.load_word2vec_format(MODEL_PATH, binary=True)

    def __getattr__(self, attr):
        """
        try to find undefined methods in the data object
        """
        return getattr(self.data, attr)

    def from_vector(self, vector):
        return self.data.similar_by_vector(vector, topn=1)[0]

    def from_word(self, word):
        if word not in self.data.vocab:
            word = get_close_matches(word.replace(" ", "_"), self.data.vocab.keys(), n=1, cutoff=0.0)[0]
        return self.data.get_vector(word)

    def doesnt_match(self, *args):
        """
        find the 'odd one out' - the word which fits the least
        """
        if len(args) == 1 and isinstance(args[0], list):
            args = args[0]
        return self.data.doesnt_match(args)

    def most_similar(self, positive=[], negative=[], topn=10):
        return self.data.most_similar(positive=positive, negative=negative, topn=topn)
