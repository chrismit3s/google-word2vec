# todo

* Word class to perform basic arithmetic with words:

    Word("New_York") - Word("USA") + Word("Germany") == Word("Berlin")

* find a way to find words which are not directly in the vocab ("new york" --> "New_York")
  * difflib
  * https://stackoverflow.com/a/3296782
